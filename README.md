This container image is meant for testing other container images
(usually built via Gitlab CI) in a setup that mirrors what is used by
[float](https://git.autistici.org/ai3/float), i.e. a root-less,
read-only Podman environment.

The image itself is based on the official Podman stable image, and it
includes a wrapper script called *with-container*, that will start a
test container and then run a test script.

It can be used in your *.gitlab-ci.yml* file like this:

```yaml
- container_test:
    stage: test
    image: registry.git.autistici.org/ai3/docker/test/float-like-podman:master
    script:
      - with-container $IMAGE_TAG ./my-test-script.sh
```

The wrapper accepts Podman options, to customize the environment (like
*--env* or *--mount*) in order for the test to run properly. Note that
these options must be specified before the IMAGE_TAG, and must follow
the --VAR=VALUE syntax strictly (each must be a single argument to
with-container in order to be parsed correctly). E.g.:

```shell
with-container --env=FOO=bar --mount=type=bind,source=config.txt,destination=config.txt my-image ./test.sh
```

### Test script interface

The with-container wrapper takes care of running the test script
multiple times in case of transient errors, so you don't have to
add retry logic to your test script.

To distinguish permanent and transient errors, with-container looks
at the test script exit status code:

* 0: success
* 1: transient failure, retry
* anything else: permanent failure

### netcat-expect

An additional little tool is provided that can run request/response
matches against a target TCP server (using *netcat*). For each test,
create a directory with files named *request* and *response* that
should match the expected interaction, and run (e.g. with respect to
the above Gitlab CI example):

```yaml
    script:
      - with-container $IMAGE_TAG netcat-expect testdata/ping localhost 8080
```
