FROM quay.io/podman/stable

# Include some tools that might be useful for testing purposes.
RUN dnf install -y netcat curl openssl

COPY with-container /usr/bin/with-container
COPY netcat-expect /usr/bin/netcat-expect
